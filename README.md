# MELODY

Matrix Equations for LOngitudinal beam DYnamics calculations (MELODY). Framework for evaluation of longitudinal beam stability by solving linearized Vlasov equation. 

Two methods of solving linearized Vlasov equation are implemented based on application:
- Oide-Yokoya discretization [K. Oide and K. Yokoya, Longitudinal single bunch instability in electron storage rings, Technical Report No. KEK-Preprint-90-10, KEK, Tsukuba, Japan, 1990]
- Direct solution of Lebedev equation in its matrix form [A. N. Lebedev, Coherent synchrotron oscillations in the
presence of a space charge, At. Energ. 25, 851 (1968)]
- Orthogonal polynomial expansion including the potential-well distortion [Y. Cai, Linear theory of microwave instability in electron storage rings, Phys. Rev. ST Accel. Beams 14, 061002 (2011)]

The main equations, which are implemented in MELODY can be found at https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.24.011002 and https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.26.014401

Related publications:
- I. Karpov, T. Argyropoulos, and E. Shaposhnikova, Thresholds for loss of Landau damping in longitudinal plane, Phys. Rev. Accel. Beams 24, 011002 (2021).
- I. Karpov, Longitudinal mode-coupling instabilities of proton bunches in the CERN Super Proton Synchrotron, Phys. Rev. Accel. Beams 26, 014401 (2023)


Presently in the development stage to become available for the public.